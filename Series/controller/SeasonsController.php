<?php 

require_once(__DIR__.'/../model/db/SeasonDb.php');


class SeasonsController{
    
    public function getSeasonById($idseason){
        $db = new SeasonDb();
        return $db->getSeasonById($idseason);
    }
    
    public function getSeasonsBySerie($idserie){
        $db = new SeasonDb();
        return $db->listSeasonsBySerie($idserie);
    }
    
    public function createSeason($y, $nc, $seas, $serieid){
        $db = new SeasonDb();
        return $db->insertSeason($y, $nc, $seas, $serieid);
    }
    
    public function updateSeason($year, $nchap, $seas, $seasonid){
        $db = new SeasonDb();
        return $db->updateSeason($year, $nchap, $seas, $seasonid);
    }
    
    public function deleteSeason($seasonid){
        $db = new SeasonDb();
        return $db->deleteSeason($seasonid);
    }
    
}