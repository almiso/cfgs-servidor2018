<?php 

class Serie{
    
    private $_sid;
    private $_title;
    private $_year;
    private $_major;
    private $_nseasons;
    
    public function __construct($t, $y, $m, $ns=null, $id = null){
        $this->setTitle($t);
        $this->setYear($y);
        $this->setMajor($m);
        $this->setNseasons($ns);
        $this->setSid($id);
    }
    
    public function getSid(){
        return $this->_sid;
    }

    public function getTitle(){
        return $this->_title;
    }

    public function getYear(){
        return $this->_year;
    }

    public function getMajor(){
        return $this->_major;
    }

    public function getNseasons(){
        return $this->_nseasons;
    }
    
    public function setSid($_sid){
        $this->_sid = $_sid;
    }

    public function setTitle($_title){
        $this->_title = $_title;
    }

    public function setYear($_year){
        $this->_year = $_year;
    }

    public function setMajor($_major){
        $this->_major = $_major;
    }

    public function setNseasons($_nseasons){
        $this->_nseasons = $_nseasons;
    }
    
}