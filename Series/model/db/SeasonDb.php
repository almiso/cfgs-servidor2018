<?php 

require_once(__DIR__.'/SerieDb.php');
require_once(__DIR__.'/../Season.php');
require_once(__DIR__.'/../Serie.php');
require_once(__DIR__.'/../../inc/Constants.php');

class SeasonDb{
    
    private $conn;
    
    public function getSeasonById($idseason){
        $this->openConnection();
        
        $sql = "SELECT * FROM season WHERE idseason = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $ids);
        $ids = $idseason;
        
        $stm->execute();
        $result = $stm->get_result();
        
        $row = $result->fetch_assoc();
        $season = new Season($row['year'],$row['nchapters'],$row['season'],
            $row['idseason'],null);
            
        $dbserie = new SerieDb();
        $serieinstance = $dbserie->getSerie($row['idserie']);
        $season->setSerie($serieinstance);
        
        return $season;
    }
    
    public function listSeasonsBySerie($idserie){
        $dbserie = new SerieDb();
        $serieinstance = $dbserie->getSerie($idserie);
        
        $this->openConnection();
        
        $sql = "SELECT * FROM season WHERE idserie = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $sid);
        $sid = $idserie;
        
        $stm->execute();
        $result = $stm->get_result();
        
        $ret = array();
        while($row = $result->fetch_assoc()){
            $season = new Season($row['year'],$row['nchapters'],$row['season'],
                $row['idseason'],$serieinstance);
            array_push($ret, $season);
        }
        return $ret;
    }
    
    public function insertSeason($y, $nc, $seas, $serieid){
        $this->openConnection();
        
        $sql = "INSERT INTO season (idserie, year, nchapters, season) VALUES (?, ?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("iiis", $sids, $sy, $snc, $sseas);
        $sids = $serieid;
        $sy = $y;
        $snc = $nc;
        $sseas = $seas;
        
        $stm->execute();
        
        
        return null;
        
    }
    
    public function updateSeason($year, $nchap, $seas, $seasonid){
        $this->openConnection();
        
        $sql = "UPDATE season SET year = ?, nchapters = ?, season = ? WHERE idseason = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("iisi", $sy, $snc, $ss, $ids);
        $sy = $year;
        $snc = $nchap;
        $ss = $seas;
        $ids = $seasonid;
        
        $stm->execute();
        
        return $this->getSeasonById($seasonid);
    }
    
    public function deleteSeason($seasonid){
        $season = $this->getSeasonById($seasonid);
        $this->openConnection();
        
        $sql = "DELETE FROM season WHERE idseason = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $sid);
        $sid = $seasonid;
        
        $stm->execute();
        
        return $season;
    }
    
    
    /**
     * Helper function to connect to db server
     *
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }
    
}