<?php 

require_once(__DIR__.'/../Serie.php');
require_once(__DIR__.'/../../inc/Constants.php');


class SerieDb{
    
    private $conn;
    
    public function getSerie($id){
        $this->openConnection();
        
        $sql = "SELECT * FROM seriesnseasons WHERE sid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $sid);
        $sid = $id;
        
        $stm->execute();
        $result = $stm->get_result();
        
        $r = $result->fetch_assoc();
        $serie = new Serie($r['title'], $r['year'],
            $r['major'], $r['nseasons'], $r['sid']);
        
        return $serie;
    }
    
    public function getSeries(){
        $this->openConnection();
        
        $sql = "SELECT * FROM seriesnseasons";
        $stm = $this->conn->prepare($sql);
        
        $stm->execute();
        $result = $stm->get_result();
        
        $ret = array();
        while($r = $result->fetch_assoc()){
            $serie = new Serie($r['title'], $r['year'],
                    $r['major'], $r['nseasons'], $r['sid']);
            array_push($ret, $serie);
        }
        return $ret;
    }
    
    public function insertSerie($t, $y, $m){
        $this->openConnection();
        
        $sql = "INSERT INTO serie (title, year, major) VALUES (?, ?, ?)";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sis", $st, $sy, $sm);
        $st = $t;
        $sy = $y;
        $sm = $m;
        
        $stm->execute();
       
        
        return new Serie($t, $y, $m, null);
        
    }
    
    public function updateSerie($t, $y, $m, $i){
        $this->openConnection();
        
        $sql = "UPDATE serie SET title = ?, year = ?, major = ? WHERE sid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("sisi", $st, $sy, $sm, $si);
        $st = $t;
        $sy = $y;
        $sm = $m;
        $si = $i;
        
        $stm->execute();
        
        return $this->getSerie($i);
    }
    
    public function deleteSerie($id){
        $serie = $this->getSerie($id);
        $this->openConnection();
        
        $sql = "DELETE FROM serie WHERE sid = ?";
        $stm = $this->conn->prepare($sql);
        
        $stm->bind_param("i", $sid);
        $sid = $id;
        
        $stm->execute();
        
        return $serie;
    }

    /**
     * Helper function to connect to db server
     * 
     */
    private function openConnection(){
        if($this->conn == null){
            $this->conn = mysqli_connect(Constants::$DB_HOST,
                Constants::$DB_USER,
                Constants::$DB_PASSWORD,
                Constants::$DB_DB);
        }
    }
 
}