<?php

require_once(__DIR__.'/../model/DateResults.php');

class DateResultsController{
    
    public $possibleCodes = ['1', 'X', '2'];
    public $possibleTeams = ['Barcelona', 'RMadrid', 'AtMadrid', 'Espanyol', 'Valencia', 'Sevilla',
                            'Bilbao', 'Betis', 'Huesca', 'Girona', 'Valladolid', 'Leganes', 'Levante',
                            'Getafe', 'Mallorca', 'Villareal', 'Oviedo', 'Salamanca', 'Zaragoza',
                            'Sporting', 'Malaga', 'Eibar', 'Depor', 'Alaves', 'RSociedad', 'Las Palmas',
                            'RayoV', 'Recreativo', 'Tenerife', 'Celta' ];
    public $removedTeams = array();
    
    public function getMoreDateResults($numd){
        $multipledates = array();
        for($i = 0 ; $i < $numd ; $i++){
            $d = $this->getDateResults($i+1);
            array_push($multipledates, $d);
            $this->possibleTeams = $this->removedTeams;
            $this->removedTeams = array();
        }
        return $multipledates;
    }
    
    public function getDateResults($dater){
        $ress = array();
        
        for($i = 0 ; $i < 15 ; $i++){
            $indext1 = rand(0,count($this->possibleTeams)-1);
            $t1 = new Team($this->possibleTeams[$indext1]);
            array_push($this->removedTeams, $this->possibleTeams[$indext1]);
            unset($this->possibleTeams[$indext1]);
            $this->rebuildArray();
            
            $indext2 = rand(0,count($this->possibleTeams)-1);
            $t2 = new Team($this->possibleTeams[$indext2]);
            array_push($this->removedTeams, $this->possibleTeams[$indext2]);
            unset($this->possibleTeams[$indext2]);
            $this->rebuildArray();
            
            $r1 = new Result($t1, $t2);
            
            $r1->setCode($this->possibleCodes[rand(0, count($this->possibleCodes
            )-1)]);
            array_push($ress, $r1);
        }
        
        $num = $dater;
        $dr = new DateResults($num, $ress);
        
        return $dr;
    }
    
    private function rebuildArray(){
        $newteams = array();
        foreach($this->possibleTeams as $team){
            array_push($newteams, $team);
        }
        $this->possibleTeams = $newteams;
    }
    
}