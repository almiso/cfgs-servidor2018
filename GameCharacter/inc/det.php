<?php 
$speeds = [ 1 => "Slow", 2 => "Very Slow", 3 => "Normal", 
            4 => "Quick", 5 => "Very Quick" ];
?>
			<ul>
    			<li>Name:<?=$gc->getName()?></li>
				<li>Specie:<?=$gc->getSpecie()?></li>
				<li>Attack Points:<?=$gc->getAttackP()?></li>
				<li>Defense Points:<?=$gc->getDefenseP()?></li>
				<li>Speed:<?=$speeds[$gc->getSpeed()]?></li>
    		</ul>