<?php 

require_once(__DIR__.'/../GameC.php');

class GameCDb{
    
    private $conn;
    
    /**
     * Create a DB registry given a GameC instance
     * 
     * @param GameC $gamec
     * @return boolean
     */
    public function createGameC($gamec){
        //Open connection
        $this->openConnection();
        
        //Prepare statement
        $insert = "INSERT INTO GameC (name, specie, attackP, defenseP, speed) VALUES (?, ?, ?, ?, ?)";
        $stmt = $this->conn->prepare($insert);
        
        //Binding params
        $stmt->bind_param("ssiii", $n, $s, $a, $d, $v);
        $n = $gamec->getName();
        $s = $gamec->getSpecie();
        $a = $gamec->getAttackP();
        $d = $gamec->getDefenseP();
        $v = $gamec->getSpeed();
        
        //Execute
        $stmt->execute();
        
        return true;
    }
    
    /**
     * List all GameC registries
     * 
     * @return array of GameC instances
     */
    public function listGameC(){
        
        //Open connection
        $this->openConnection();
        
        //Prepare statement
        $query = "SELECT * FROM GameC";
        $stmt = $this->conn->prepare($query);
        
        //Execute
        $stmt->execute();
        $res = $stmt->get_result();
        
        //Processing results
        $result = array();
        while ($gc = $res->fetch_assoc() ) {
            //Create instance
            $gcinstance = new GameC($gc['name'], $gc['specie'],
                    $gc['defenseP'], $gc['attackP'], 
                    $gc['speed']);
            $gcinstance->setId($gc['gcid']);
            array_push($result, $gcinstance);
        }
        return $result;
    }
    
    /**
     * Get a GameC instance given its ID
     * 
     * @param Integer $ip
     * @return GameC instance
     */
    public function getGameC($ip){
        
        //Open connection
        $this->openConnection();
        
        //Prepare statement
        $query = "SELECT * FROM GameC WHERE gcid = ? ";
        $stmt = $this->conn->prepare($query);
        
        //Binding param
        $stmt->bind_param("i", $id);
        $id = $ip;
        
        //Execute
        $stmt->execute();
        $res = $stmt->get_result();
        
        //Processing results
        while ($gc = $res->fetch_assoc() ) {
            $gcinstance = new GameC($gc['name'], $gc['specie'],
                    $gc['defenseP'], $gc['attackP'],
                    $gc['speed']);
            $gcinstance->setId($gc['gcid']);
        }
        return $gcinstance;
    }
    
    /**
     * Remove a GameC registry and return the original data
     * 
     * @param Integer $id
     * @return GameC instance with original data
     */
    public function removeGameC($id){
        //Get instance
        $gc = $this->getGameC($id);
        
        //Open connection
        $this->openConnection();
        
        //Prepare statement
        $query = "DELETE FROM GameC WHERE gcid = ? ";
        $stmt = $this->conn->prepare($query);
        
        //Binding param
        $stmt->bind_param("i", $gcid);
        $gcid = $id;
        
        //Execute
        $stmt->execute();
        
        return $gc;
    }
    
    /**
     * Helper method to open connection with the database
     * 
     */
    private function openConnection () {
        if($this->conn == null){
            $this->conn = mysqli_connect("127.0.0.1", 
                                        "game", 
                                        "cfgs!DAW2018", 
                                        "game");
        }
    }
    
}