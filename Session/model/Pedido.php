<?php 

class Pedido{
    
    private $_idpedido; 
    private $_restaurante;
    private $_primero;
    private $_segundo;
    private $_postre;
    private $_precio;
    private $_estado;
    private $_creacion;
    
    public function __construct($id, $re, $pri, $se, 
            $po, $pre, $es, $cr){
        $this->setIdpedido($id);
        $this->setRestaurante($re);
        $this->setPrimero($pri);
        $this->setSegundo($se);
        $this->setPostre($po);
        $this->setPrecio($pre);
        $this->setEstado($es);
        $this->setCreacion($cr);
    }
    
    public function getIdpedido()
    {
        return $this->_idpedido;
    }

    public function getRestaurante()
    {
        return $this->_restaurante;
    }

    public function getPrimero()
    {
        return $this->_primero;
    }

    public function getSegundo()
    {
        return $this->_segundo;
    }

    public function getPostre()
    {
        return $this->_postre;
    }

    public function getPrecio()
    {
        return $this->_precio;
    }

    public function getEstado()
    {
        return $this->_estado;
    }

    public function getCreacion()
    {
        return $this->_creacion;
    }

    public function setIdpedido($_idpedido)
    {
        $this->_idpedido = $_idpedido;
    }

    public function setRestaurante($_restaurante)
    {
        $this->_restaurante = $_restaurante;
    }

    public function setPrimero($_primero)
    {
        $this->_primero = $_primero;
    }

    public function setSegundo($_segundo)
    {
        $this->_segundo = $_segundo;
    }

    public function setPostre($_postre)
    {
        $this->_postre = $_postre;
    }

    public function setPrecio($_precio)
    {
        $this->_precio = $_precio;
    }

    public function setEstado($_estado)
    {
        $this->_estado = $_estado;
    }

    public function setCreacion($_creacion)
    {
        $this->_creacion = $_creacion;
    }
    
}