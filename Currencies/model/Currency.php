<?php 

class Currency{
    
    private $_name;
    private $_symbol;
    private $_eurval;
    private $_type;
    
    public function __construct($n = null, $s = null,
            $ev = null, $t = null){
        $this->setName($n);
        $this->setSymbol($s);
        $this->setEurval($ev);
        $this->setType($t);
    }
    
    public function getName()
    {
        return $this->_name;
    }

    public function getSymbol()
    {
        return $this->_symbol;
    }

    public function getEurval()
    {
        return $this->_eurval;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setName($_name)
    {
        $this->_name = $_name;
    }

    public function setSymbol($_symbol)
    {
        $this->_symbol = $_symbol;
    }

    public function setEurval($_eurval)
    {
        $this->_eurval = $_eurval;
    }

    public function setType($_type)
    {
        $this->_type = $_type;
    }

    
    
    
}