<?php 

require_once(__DIR__.'/../model/db/CurrencyDb.php');

class IndexController{
    
    public function getCurrencies(){
        $db = new CurrencyDb();
        return $db->getCurrencies();
    }
    
    public function getCurrency($symbol){
        $db = new CurrencyDb();
        return $db->getCurrency($symbol);
    }
    
    public function createCurrency($cn, $cs, $cev, $ct){
        $db = new CurrencyDb();
        return $db->createCurrency($cn, $cs, $cev, $ct);
    }
    
    public function updateCurrency($co, $cn, $cs, $cev, $ct){
        $db = new CurrencyDb();
        return $db->createCurrency($co, $cn, $cs, $cev, $ct);
    }
    
}