<?php

class Code{
    
    private $_code;
    private $_val;
    
    public function __construct($c = "", $v = ""){
        $this->setCode($c);
        $this->setVal($v);
    }
    
    public function getCode(){
        return $this->_code;
    }
    
    public function getVal(){
        return $this->_val;
    }
    
    public function setCode($c){
        /*
        if($c[0] != 'A' || $c[0] != 'a'){
            return false;
        }
        if(!is_int(substr($c, 1))){
            return false;
        }
        */
        $this->_code = $c;
    }
     
    public function setVal($v){
        $sec = split(".", $v);
        if(is_int($sec[0]) && is_int($sec[1]) && is_int($sec[2])){
            $this->_val = $v;    
        }else{
            return false ;
        }
        
    }   
    
}