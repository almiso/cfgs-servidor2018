<?php

require_once(__DIR__.'/../controller/IndexController.php');
$cnt = new IndexController();
$list = $cnt->generateList();

?><html>
    <head>
        <title>Codes</title>    
    </head>
    <body>
        <div id="wrapper">
            <div id="inputform">
                <form method="get" action="/forms/index.php">
                    <dl>
                        <dl><label for="codein">Code</label></dl>
                        <dd><input type="text" name="code" id="codein" tabindex="1"/></dd>
                        <dl><label for="valuein">Value</label></dl>
                        <dd><input type="text" name="value" id="valuein" tabindex="2"/></dd>
                        <dd><input type="submit" name="submitform" value="Enviar"/></dd>
                    </dl>
                </form>
            </div>
            <h1>Codes</h1>
            <table>
                <tr><th>Code</th><th></th></tr>
                <?php foreach($list as $l) { ?>
                <tr>
                    <td><?=$l->getCode()?></td>
                    <td><a href="/details.php?code=<?=$l->getCode()?>">Ver detalles</a></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </body>
    
    
</html>