<?php 

class Persona{
    
    private $_name;
    private $_lastname;
    private $_age;
    
    public function __construct($n="", $l="", $a=0){
        $this->setAge($a);
        $this->setName($n);
        $this->setLastname($l);
    }
    
    public function setName($n){
        $this->_name = $n;
    }
    
    public function setLastname($n){
        $this->_lastname = $n;
    }
    
    public function setAge($n){
        $this->_age = $n;
    }
    
    public function getName(){
        return $this->_name;
    }
    
    public function getLastname(){
        return $this->_lastname;
    }
    
    public function getAge(){
        return $this->_age;
    }
    
    
    
}